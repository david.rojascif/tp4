Résultat Valgrind:
Aucun memory leak
(Dans une version antérieure du code, un memory leak aurait été détecté
dans le code d'IntervalTree.h car la méthode fromIntervals retournait 
une adresse mémoire qui se libérait après la fin de la méthode.

Cette erreur a été corrigée en faisant que le code retourne une adresse mémoire
 allouée par le heap.)

Sortie console de benchmark avec Valgrind:
Finished calculation (1000) in avg.     2761ms, total   13808ms
Finished calculation (30000) in avg.    1572778ms, total        7863891ms
Finished calculation (30000) in avg.    1539109ms, total        7695545ms
Finished calculation (30000) in avg.    2226014ms, total        11130070ms


Sortie console de benchmark avant optimisation:

Finished calculation (1000) in avg.     54ms, total     273ms
Finished calculation (30000) in avg.    24710ms, total  123554ms
Finished calculation (30000) in avg.    27004ms, total  135020ms
Finished calculation (30000) in avg.    39749ms, total  198748ms

Sortie console de benchmark après optimisation:
Finished calculation (1000) in avg.     29ms, total     145ms
Finished calculation (30000) in avg.    19906ms, total  99533ms
Finished calculation (30000) in avg.    22541ms, total  112709ms
Finished calculation (30000) in avg.    39788ms, total  198940ms