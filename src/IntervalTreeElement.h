//
// Created by beatw on 4/29/2022.
//

#ifndef CPPALGO_INTERVALTREEELEMENT_H
#define CPPALGO_INTERVALTREEELEMENT_H

#include <vector>
#include <algorithm>
#include "Interval.h"

/**
 * Single element of the interval tree
 * @tparam T
 */
template<typename T>
class IntervalTreeElement {
public:
    /**
     * Building IntervalTreeElement
     * @param values
     * @param mid
     */
    IntervalTreeElement(std::vector<Interval<T>> &values, T mid) : mid(mid){

        //fill attributes
        rightSorted=values;
        leftSorted=values;

        std::sort(rightSorted.begin(),rightSorted.end(),[](const Interval<T> interval1,const Interval<T> interval2){
            return interval1.getValue()<interval2.getValue();
        });
        std::sort(leftSorted.begin(),leftSorted.end(),[](const Interval<T> interval1,const Interval<T> interval2){
            return interval1.getValue()>interval2.getValue();
        });

    }

    /**
     * Return all Intervals that intersect with a given point
     * @param x
     * @return
     */
    std::vector<Interval<T>> intersecting(T x) {
        std::vector<Interval<T>> intersectInterval;
        for(Interval<T> interval:rightSorted){
            if (interval.contains(x))intersectInterval.push_back(interval);
        }
        return intersectInterval;
    }

    std::vector<Interval<T>> leftSorted;
    std::vector<Interval<T>> rightSorted;
    T mid;
};

#endif //CPPALGO_INTERVALTREEELEMENT_H
