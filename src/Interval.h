//
// Created by Beat Wolf on 30.11.2021.
//

#ifndef CPPALGO_INTERVAL_H
#define CPPALGO_INTERVAL_H


template<typename RangeType=int,typename ValueType=double>
class Interval{
public:
    Interval(RangeType start, RangeType end, ValueType value=0): start_(start), end_(end), value_(value){};
    RangeType getStart()const{
        return start_;
    }
    RangeType getEnd()const{
        return end_;
    }
    ValueType getValue()const{
        return value_;
    }

    bool contains(ValueType value)const{
        return this->start_ <= value&& value <= this->end_;
    }

    bool operator==(const Interval &other){
        /*if (this->value_ != other.value_)return false;
        RangeType startCompare= this->start_ - other.start_;
        if (startCompare<0)startCompare*=-1;
        RangeType endCompare= this->end_ - other.end_;
        if (endCompare<0)endCompare*=-1;
        auto epsilon=std::numeric_limits<RangeType>::epsilon();
        return startCompare<epsilon&&endCompare<epsilon;*/
        auto valueComparison= std::abs(this->value_-other.value_);
        return valueComparison<=std::numeric_limits<RangeType>::epsilon();
    }

    bool operator!=(const Interval &other){
        /*if (this->value_ == other.value_)return false;
        RangeType startCompare= this->start_ - other.start_;
        if (startCompare<0)startCompare*=-1;
        RangeType endCompare= this->end_ - other.end_;
        if (endCompare<0)endCompare*=-1;
        auto epsilon=std::numeric_limits<RangeType>::epsilon();
        return startCompare>=epsilon&&endCompare>=epsilon;*/
        auto valueComparison= std::abs(this->value_-other.value_);
        return valueComparison > std::numeric_limits<RangeType>::epsilon();
    }
private:
    RangeType start_;
    RangeType end_;
    ValueType value_;
};
#endif //CPPALGO_INTERVAL_H
