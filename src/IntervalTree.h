//
// Created by Beat Wolf on 07.07.2021.
//

#ifndef CPPALGO_INTERVALTREE_H
#define CPPALGO_INTERVALTREE_H

#include <vector>
#include <algorithm>

#include "base/BinaryTree.h"
#include "IntervalTreeElement.h"


/**
 * Binary interval tree
 * @tparam T
 */
template<typename T>
class IntervalTree {
public:
    IntervalTree(const std::vector<Interval<T>> &intervals);

    /**
     * Number of intervals in the interval tree
     * @return
     */
    size_t size() const;

    /**
     * Returns all intervals that contain a given point
     * @tparam T
     * @param x
     * @return
     */
    std::vector<Interval<T>> containing(T x) const;

private:
    void
    intersecting(std::vector<Interval<T>> &solution, BinaryTreeNode<IntervalTreeElement<T>, T> *node, T x) const;

    BinaryTreeNode<IntervalTreeElement<T>, T> *
    fromIntervals(const std::vector<Interval<T>> &intervals, BinaryTreeNode<IntervalTreeElement<T>, T> *parent);

    BinaryTree<IntervalTreeElement<T>, T> tree;
    T getMid(const std::vector<Interval<T>> &intervals);
};


template<typename T>
IntervalTree<T>::IntervalTree(const std::vector<Interval<T>> &intervals) {
    tree.setRoot(fromIntervals(intervals, nullptr));
}

template<typename T>
size_t IntervalTree<T>::size() const {
    return tree.size();
}

template<typename T>
std::vector<Interval<T>> IntervalTree<T>::containing(T x) const {
    std::vector<Interval<T>> solution;
    intersecting(solution, tree.getRoot(), x);
    return solution;
}

template<typename T>
void IntervalTree<T>::intersecting(std::vector<Interval<T>> &solution,
                                   BinaryTreeNode<IntervalTreeElement<T>, T> *node, T x) const {

    if (node == nullptr) {
        return;
    }

    //Add intervals from current node that intersect
    for(const Interval<T> interval:node->value.intersecting(x)){
        solution.push_back(interval);
    }
    //Handle left/right children
    intersecting(solution,node->left,x);
    intersecting(solution,node->right,x);
}

template<typename T>
BinaryTreeNode<IntervalTreeElement<T>, T> *
IntervalTree<T>::fromIntervals(const std::vector<Interval<T>> &intervals,
                               BinaryTreeNode<IntervalTreeElement<T>, T> *parent) {
    if (intervals.empty()) {
        return nullptr;
    }

    T mid=getMid(intervals);
    //Split intervals to left, mid, right
    //Create new BinaryTreeNode
    //Initialize/Build left and right child of that new node
    //Return the new node
    std::vector<Interval<T>> midPart;
    std::vector<Interval<T>> leftPart;
    std::vector<Interval<T>> rightPart;

    for (const Interval<T> &interval: intervals) {
        if (interval.getEnd()<mid){
            leftPart.push_back(interval);
        }else if(interval.getStart()>mid){
            rightPart.push_back(interval);
        }else if (interval.contains(mid)){
            midPart.push_back(interval);
        }
    }
    /*
    T leftMid=getMid(leftPart);
    T rightMid= getMid(rightPart);
    IntervalTreeElement<T> midElement(midPart,mid);
    IntervalTreeElement<T> leftElement(leftPart,leftMid);
    IntervalTreeElement<T> rightElement(rightPart,rightMid);


    BinaryTreeNode<IntervalTreeElement<T>, T> binaryTreeNode(midElement.mid,midElement,parent);
    binaryTreeNode.insert(leftElement.mid,leftElement);
    binaryTreeNode.insert(rightElement.mid,rightElement);
    auto* treeNode=new BinaryTreeNode<IntervalTreeElement<T>, T> (binaryTreeNode);
    return treeNode;*/
    auto* treeNode= new BinaryTreeNode<IntervalTreeElement<T>,T>(mid,IntervalTreeElement<T>(midPart,mid),parent);
    treeNode->left= fromIntervals(leftPart,treeNode);
    treeNode->right= fromIntervals(rightPart,treeNode);
    return treeNode;
}

template<typename T>
T IntervalTree<T>::getMid(const std::vector<Interval<T>> &intervals){
    std::vector<T> points;
    for (const Interval<T> &interval: intervals) {
        points.push_back(interval.getStart());
        points.push_back(interval.getEnd());
    }
    std::sort(points.begin(), points.end());

    return points.at(intervals.size()); // so it's the median (of 2n values)

}


#endif //CPPALGO_INTERVALTREE_H
