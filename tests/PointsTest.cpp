//
// Created by david on 16/05/2022.
//

#include "../includes/Catch2/catch.hpp"
#include "Point.h"

TEST_CASE("Point addition"){
int p1x=3;
int p1y=4;
int p2x=-p1x;
int p2y=2*p1y;
Point p1(p1x,p1y);
Point p2(p2x,p2y);
Point p3=p1+p2;
REQUIRE(p3.getX()==0);//test addition of negative and positive numbers
REQUIRE(p3.getY()/3==p1.getY());//test addition logic with division
Point p4(p1);
p4+=p3;
REQUIRE(p4.getY()==4*p1.getY());//test addition logic with multiplication
}

TEST_CASE("Point subtraction"){
int p1x=3;
int p1y=4;
int p2x=-p1x;
int p2y=2*p1y;
Point p1(p1x,p1y);
Point p2(p2x,p2y);
Point p3=p1-p2;
REQUIRE(p3.getY()<0);//test passage from positive to negative
REQUIRE(p3.getX()==2*p1.getX());//test subtraction of negative number
Point p4(p2);
p4-=p3;
REQUIRE(p4.getX()==2*p2.getX()-p1.getX());//test subtraction logic
}

TEST_CASE("Point division"){
int px=12;
int py=16;
double divisor=2;
Point p(px, py);
p/=divisor;
REQUIRE(p.getX() == px / divisor);//test check value
REQUIRE(p.getY() == py / divisor);
divisor=0;
REQUIRE_THROWS(p/=divisor);//test division by zero exception
divisor=2;
p= p / divisor;
REQUIRE_FALSE(p.getX() > px);
}

TEST_CASE("Point multiplication"){
int p1x=2;
int p1y=3;
double factor=2;
Point p1(p1x,p1y);
Point p2(p1);
p1*=factor;
REQUIRE(p1.getX()/p2.getX()==factor);//test division to find multiplication factor
REQUIRE(p1.getY()/p2.getY()==factor);
p2=p2*factor;
REQUIRE(p2.getX()==p1.getX());
REQUIRE(p2.getY()==p1.getY());
}

TEST_CASE("Point distance test"){
int p1x=5;
int p1y=9;
int p2x=8;
int p2y=5;
Point p1(p1x,p1y);
Point p2(p2x,p2y);
int expectedDistance=5;
REQUIRE(p1.distance(p2)==expectedDistance);
REQUIRE(p2.distance(p1)==expectedDistance);
}

TEST_CASE("Point setters test"){
int px=9;
int py=2;
Point p(px,py);
p.setX(p.getY());
REQUIRE(p.getX()==p.getY());//test equal values
}